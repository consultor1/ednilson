﻿namespace Teste_Envolti_Ednilson_C_Tavares_20190221.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    [Table("Servico")]
    public partial class Servico
    {
        [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [ForeignKey("Fornecedor"), Column(Order = 1)]
        [Display(Name = "Fornecedor")]
        public int Fornecedor_Id { get; set; }
        
        [Required]
        [ForeignKey("Cliente"), Column(Order = 2)]
        [Display(Name = "Cliente")]
        public int Cliente_Id { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Descricao")]
        public string Descricao { get; set; }

        [Display(Name = "Data do Atendimento")]
        public DateTime DataAtendimento { get; set; }

        [Required]
        [Display(Name = "Valor")]
        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public decimal Valor { get; set; }

        [Required]
        [Display(Name = "Tipo")]
        [Range(1, int.MaxValue, ErrorMessage = "Selecione um tipo de serviço.")]
        public int TipoServiço { get; set; }

        public virtual Fornecedor Fornecedor { get; set; }
        public virtual Cliente Cliente { get; set; }

    }
}

