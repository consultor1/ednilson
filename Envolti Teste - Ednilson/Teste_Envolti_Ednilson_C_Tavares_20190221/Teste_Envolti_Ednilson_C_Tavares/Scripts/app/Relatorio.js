﻿function PesquisaRankMensal() {

    var url = baseUrl + "Relatorio/GetPartialRankMensal";

    $.get(
        url,
        {},
        function (data) {
            $("#divrelatorio").empty();
            $("#divrelatorio").html(data);
        });
}

function PesquisaMediaFornecedor() {

    var url = baseUrl + "Relatorio/GetPartialMediaFornecedor";

    $.get(
        url,
        {},
        function (data) {
            $("#divrelatorio").empty();
            $("#divrelatorio").html(data);
        });
}

function PesquisaFrnSemServico() {

    var url = baseUrl + "Relatorio/GetPartialFrnSemServico";

    $.get(
        url,
        {},
        function (data) {
            $("#divrelatorio").empty();
            $("#divrelatorio").html(data);
        });
}
