﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Teste_Envolti_Ednilson_C_Tavares_20190221.Models;

namespace Teste_Envolti_Ednilson_C_Tavares_20190221.Controllers
{
    public class ServicoController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Servico
        public ActionResult Index()
        {
            ViewBag.ClienteList = db.Cliente.ToList();

            var dttipos = new DataTable();
            dttipos.Columns.Add("Id");
            dttipos.Columns.Add("Tipo");

            dttipos.Rows.Add(1, "Conserto eletrônico");
            dttipos.Rows.Add(2, "Serviços gerais");
            dttipos.Rows.Add(3, "Manutenção hidráulica");
            dttipos.Rows.Add(4, "instalação elétrica");
            dttipos.Rows.Add(5, "jardinagem");

            ViewBag.TipoServico = dttipos.AsDataView();

            return View();
        }

        // GET: GetPartialListaServico
        public ActionResult GetPartialListaServico(
            int? clienteId,
            string estado,
            string cidade,
            string bairro,
            int? tipoServico,
            decimal? vlMinimo,
            decimal? vlMaximo,
            string DataInicial,
            string DataFinal)
        {
            var servico = db.Servico.Include(s => s.Cliente).Include(s => s.Fornecedor);

            if (clienteId != null && clienteId > 0)
            {
                var _clienteId = Convert.ToInt32(clienteId);
                servico = servico.Where(x => x.Cliente_Id == _clienteId);
            }

            if (!string.IsNullOrWhiteSpace(estado))
                servico = servico.Where(x => x.Cliente.Estado.Contains(estado));

            if (!string.IsNullOrWhiteSpace(cidade))
                servico = servico.Where(x => x.Cliente.Cidade.Contains(cidade));

            if (!string.IsNullOrWhiteSpace(bairro))
                servico = servico.Where(x => x.Cliente.Bairro.Contains(bairro));

            if (tipoServico != null && tipoServico > 0)
            {
                var _tipoServico = Convert.ToInt32(tipoServico);
                servico = servico.Where(x => x.TipoServiço == _tipoServico);
            }

            if (vlMinimo != null && vlMinimo > 0)
            {
                var _vlMinimo = Convert.ToDecimal(vlMinimo);
                servico = servico.Where(x => x.Valor >= _vlMinimo);
            }

            if (vlMaximo != null && vlMaximo > 0)
            {
                var _vlMaximo = Convert.ToDecimal(vlMaximo);
                servico = servico.Where(x => x.Valor <= _vlMaximo);
            }

            var _DataInicial = new DateTime();

            if ( !string.IsNullOrEmpty(DataInicial))
            {
                _DataInicial = Convert.ToDateTime(DataInicial);
                servico = servico.Where(x => x.DataAtendimento >= _DataInicial);
            }

            var _DataFinal = new DateTime();

            if (!string.IsNullOrEmpty(DataFinal))
                {
                    _DataFinal = Convert.ToDateTime(DataFinal).AddDays(1).AddMinutes(-1);
                servico = servico.Where(x => x.DataAtendimento <= _DataFinal);
            }

            return View("PartialListaServico", servico.ToList());
        }

        // GET: Servico/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Servico servico = db.Servico.Find(id);
            if (servico == null)
            {
                return HttpNotFound();
            }
            return View(servico);
        }

        // GET: Servico/Create
        public ActionResult Create()
        {
            ViewBag.ClienteList = db.Cliente.ToList();
            
            var dttipos = new DataTable();
            dttipos.Columns.Add("Id");
            dttipos.Columns.Add("Tipo");

            dttipos.Rows.Add(1, "Conserto eletrônico");
            dttipos.Rows.Add(2, "Serviços gerais");
            dttipos.Rows.Add(3, "Manutenção hidráulica");
            dttipos.Rows.Add(4, "instalação elétrica");
            dttipos.Rows.Add(5, "jardinagem");

            ViewBag.TipoServico = dttipos.AsDataView();

            ViewBag.dataDefault = DateTime.Now.Date.ToString("yyyy-MM-dd");

            return View();
        }

        // POST: Servico/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Cliente_Id,Descricao,DataAtendimento,Valor,TipoServiço")] Servico servico)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId();
                var user = db.Users.FirstOrDefault(u => u.Id == userId);
                    
                servico.Fornecedor_Id = user.Fornecedor_Id;
                // servico.DataAtendimento = DateTime.Now;

                db.Servico.Add(servico);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Cliente_Id = new SelectList(db.Cliente, "Id", "Nome", servico.Cliente_Id);
            ViewBag.Fornecedor_Id = new SelectList(db.Fornecedor, "Id", "Nome", servico.Fornecedor_Id);
            return View(servico);
        }

        // GET: Servico/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Servico servico = db.Servico.Find(id);
            if (servico == null)
            {
                return HttpNotFound();
            }
            ViewBag.Cliente_Id = new SelectList(db.Cliente, "Id", "Nome", servico.Cliente_Id);
            ViewBag.Fornecedor_Id = new SelectList(db.Fornecedor, "Id", "Nome", servico.Fornecedor_Id);
            return View(servico);
        }

        // POST: Servico/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Fornecedor_Id,Cliente_Id,Descricao,DataAtendimento,Valor")] Servico servico)
        {
            if (ModelState.IsValid)
            {
                db.Entry(servico).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Cliente_Id = new SelectList(db.Cliente, "Id", "Nome", servico.Cliente_Id);
            ViewBag.Fornecedor_Id = new SelectList(db.Fornecedor, "Id", "Nome", servico.Fornecedor_Id);
            return View(servico);
        }

        // GET: Servico/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Servico servico = db.Servico.Find(id);
            if (servico == null)
            {
                return HttpNotFound();
            }
            return View(servico);
        }

        // POST: Servico/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Servico servico = db.Servico.Find(id);
            db.Servico.Remove(servico);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
