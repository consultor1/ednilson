﻿namespace Teste_Envolti_Ednilson_C_Tavares_20190221.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    [Table("Fornecedor")]
    public partial class Fornecedor
    {
        public Fornecedor()
        {
            this.Servico = new HashSet<Servico>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        public virtual ICollection<Servico> Servico { get; set; }
    }
}

