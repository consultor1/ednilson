﻿USE [BdTesteMalwee2]
GO
SET IDENTITY_INSERT [dbo].[Cliente] ON 

GO
INSERT [dbo].[Cliente] ([Id], [Nome], [Bairro], [Cidade], [Estado]) VALUES (1, N'Jorge', N'Enseada', N'Lagoa Negra', N'SC')
GO
INSERT [dbo].[Cliente] ([Id], [Nome], [Bairro], [Cidade], [Estado]) VALUES (2, N'Lucas', N'Cortesias', N'Barro Branco', N'PR')
GO
INSERT [dbo].[Cliente] ([Id], [Nome], [Bairro], [Cidade], [Estado]) VALUES (3, N'Mateus', N'Cortesias', N'Barro Branco', N'PR')
GO
SET IDENTITY_INSERT [dbo].[Cliente] OFF
GO
SET IDENTITY_INSERT [dbo].[Fornecedor] ON 

GO
INSERT [dbo].[Fornecedor] ([Id], [Nome]) VALUES (1, N'Fulano')
GO
INSERT [dbo].[Fornecedor] ([Id], [Nome]) VALUES (2, N'Ciclano')
GO
INSERT [dbo].[Fornecedor] ([Id], [Nome]) VALUES (3, N'Beltrano')
GO
SET IDENTITY_INSERT [dbo].[Fornecedor] OFF
GO
SET IDENTITY_INSERT [dbo].[Servico] ON 

GO
INSERT [dbo].[Servico] ([Id], [Fornecedor_Id], [Cliente_Id], [Descricao], [DataAtendimento], [Valor], [TipoServiço]) VALUES (1, 1, 1, N'idgvasgd', CAST(N'2019-01-09 00:00:00.000' AS DateTime), 222.2200, 1)
GO
INSERT [dbo].[Servico] ([Id], [Fornecedor_Id], [Cliente_Id], [Descricao], [DataAtendimento], [Valor], [TipoServiço]) VALUES (2, 1, 2, N'idgvasgd', CAST(N'2019-01-01 00:00:00.000' AS DateTime), 11.1100, 2)
GO
INSERT [dbo].[Servico] ([Id], [Fornecedor_Id], [Cliente_Id], [Descricao], [DataAtendimento], [Valor], [TipoServiço]) VALUES (3, 1, 2, N'lavagem', CAST(N'2019-01-01 00:00:00.000' AS DateTime), 3.0000, 2)
GO
SET IDENTITY_INSERT [dbo].[Servico] OFF
GO
