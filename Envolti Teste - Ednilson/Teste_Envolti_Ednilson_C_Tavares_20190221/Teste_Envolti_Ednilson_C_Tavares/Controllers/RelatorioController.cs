﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Teste_Envolti_Ednilson_C_Tavares_20190221.Models;

namespace Teste_Envolti_Ednilson_C_Tavares_20190221.Controllers
{
    public class RelatorioController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Relatorio
        public ActionResult Index()
        {
            return View();
        }

        // GET: Relatorio
        public ActionResult GetPartialRankMensal()
        {
            var listRankMensal = new List<RankMensal>();
            var mesExtenso = string.Empty;
            var data = string.Empty;
            var datain = new DateTime();
            var datafim = new DateTime();

            var query =
                @"select
                    distinct(cli.Nome),
                    sum(svc.Valor) as Total
                  from dbo.Servico svc
                  inner join dbo.Cliente cli ON cli.Id = svc.Cliente_Id
                  where svc.DataAtendimento >= @datain
                    and svc.DataAtendimento <= @datafim
                  group by cli.Nome
                  order by Total desc
                ;";

            var sqlConnection = new SqlConnection(db.Database.Connection.ConnectionString);

            for (int i = 1; i <= 12; i++)
            {
                datain = new DateTime(DateTime.Now.Year, i, 1);
                datafim = datain.AddMonths(1).AddDays(-1);

                var totalCliente = sqlConnection.Query<TotalCliente>(query, new { datain, datafim }).Take(3).ToList();

                if (totalCliente.Count == 0)
                    totalCliente.Add(new TotalCliente() {
                        Nome = "SEM CLIENTES",
                        Total = 0M
                    });

                mesExtenso = System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(i).ToLower();
                mesExtenso = char.ToUpper(mesExtenso[0]) + mesExtenso.Substring(1);
                
                listRankMensal.Add(new RankMensal()
                {
                    Mes = mesExtenso,
                    qtdClientes = totalCliente.Count(),
                    listTotalCliente = totalCliente
                });
            }
            return View("PartialRankMensal", listRankMensal.ToList());
        }
       
        // GET: Relatorio
        public ActionResult GetPartialMediaFornecedor()
        {
            var listServicos = 
                db.Servico
                .Include(s => s.Fornecedor)
                .GroupBy(s => new {s.Fornecedor.Nome, s.TipoServiço})
                .Select( b => new
                {
                    b.Key.Nome,
                    Tipo = b.Key.TipoServiço,
                    Media = b.Average(v => v.Valor)
                }).ToList();

            var frnAnterior = string.Empty;
            var qtdTipo = 0;

            var listMediaFornecedor = new List<MediaFornecedor>();
            var listMediaServico = new List<MediaServico>();

            for (int i = 0; i < listServicos.Count; i++)
            {
                qtdTipo += 1;
                var tipo = string.Empty;
                                               
                    if (listServicos[i].Tipo == 1)
                        tipo = "Conserto eletrônico";
                    else if (listServicos[i].Tipo == 2)
                        tipo = "Serviços gerais";
                    else if (listServicos[i].Tipo == 3)
                        tipo = "Manutenção hidráulica";
                    else if (listServicos[i].Tipo == 4)
                        tipo = "Instalação elétrica";
                    else if (listServicos[i].Tipo == 5)
                        tipo = "Jardinagem";
                    else
                        tipo = "";
                
                listMediaServico.Add(new MediaServico()
                {
                    Media = listServicos[i].Media,
                    Tipo = tipo
                });

                if (listServicos.Count == (i + 1) || listServicos[i].Nome != listServicos[i + 1].Nome)
                {
                    listMediaFornecedor.Add(new MediaFornecedor()
                    {
                        Nome = listServicos[i].Nome,
                        listMediaServico = listMediaServico,
                        qtdTipo = qtdTipo
                    });
                    listMediaServico = new List<MediaServico>();
                    qtdTipo = 0;
                }
            }

            return View("PartialMediaFornecedor", listMediaFornecedor);
        }

        // GET: Relatorio
        public ActionResult GetPartialFrnSemServico()
        {
            var listRankFrnSemServico = new List<RankFrnSemServico>();
            var mesExtenso = string.Empty;
            var data = string.Empty;
            var datain = new DateTime();
            var datafim = new DateTime();

            var query =
                @"  SELECT Nome
	                  FROM dbo.Fornecedor frn
                      left outer join dbo.Servico svc on svc.Fornecedor_Id = frn.Id 
                       and svc.DataAtendimento >= @datain
                       and svc.DataAtendimento <= @datafim
                       where svc.id is null
                    ;";

            var sqlConnection = new SqlConnection(db.Database.Connection.ConnectionString);

            for (int i = 1; i <= 12; i++)
            {
                datain = new DateTime(DateTime.Now.Year, i, 1);
                datafim = datain.AddMonths(1).AddDays(-1);

                var frnOcio = sqlConnection.Query<string>(query, new { datain, datafim }).ToList();

                mesExtenso = System.Globalization.DateTimeFormatInfo.CurrentInfo.GetMonthName(i).ToLower();
                mesExtenso = char.ToUpper(mesExtenso[0]) + mesExtenso.Substring(1);

                if (frnOcio.Count == 0)
                    frnOcio.Add("NENHUM FORNECEDOR SEM SERVIÇO");

                listRankFrnSemServico.Add(new RankFrnSemServico()
                {
                    Mes = mesExtenso,
                    listFrnSemServico = frnOcio,
                    qtdFrn = frnOcio.Count()
                });
            }
            return View("PartialRankFrnSemServico", listRankFrnSemServico);
        }
    }
}
