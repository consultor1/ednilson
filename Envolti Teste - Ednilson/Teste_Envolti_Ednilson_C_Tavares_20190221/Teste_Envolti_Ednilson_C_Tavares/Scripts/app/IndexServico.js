﻿function PesquisaServicos() {

    var c = document.getElementById("Cliente_Id");
    var clienteId = c.options[c.selectedIndex].value;

    var estado = document.getElementById("txtEstado").value;
    var cidade = document.getElementById("txtCidade").value;
    var bairro = document.getElementById("txtBairro").value;

    var t = document.getElementById("TipoServico");
    var tipoServico = t.options[t.selectedIndex].value;

    var vmin = document.getElementById("txtVlMinimo").value;
    var vlMinimo = parseFloat(vmin.replace(',', '.').replace(' ', ''));

    var vmax = document.getElementById("txtVlMaximo").value;
    var vlMaximo = parseFloat(vmax.replace(',', '.').replace(' ', ''));

    var url = baseUrl + "Servico/GetPartialListaServico";

    var DataInicial = document.getElementById("dtpFiltroDataInicial").value;
    var DataFinal = document.getElementById("dtpFiltroDataFinal").value;

    $.get(
        url,
        {
            clienteId: clienteId,
            estado: estado,
            cidade: cidade,
            bairro: bairro,
            tipoServico: tipoServico,
            vlMinimo: vlMinimo,
            vlMaximo: vlMaximo,
            DataInicial: DataInicial, 
            DataFinal: DataFinal 
        },
        function (data) {
            $("#divservico").empty();
            $("#divservico").html(data);
        });
}
