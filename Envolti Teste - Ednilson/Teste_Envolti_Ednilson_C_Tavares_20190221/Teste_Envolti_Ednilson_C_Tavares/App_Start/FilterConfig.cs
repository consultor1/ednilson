﻿using System.Web;
using System.Web.Mvc;

namespace Teste_Envolti_Ednilson_C_Tavares_20190221
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
