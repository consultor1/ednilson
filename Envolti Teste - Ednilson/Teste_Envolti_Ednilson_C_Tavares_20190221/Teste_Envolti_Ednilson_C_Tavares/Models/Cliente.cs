﻿namespace Teste_Envolti_Ednilson_C_Tavares_20190221.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    [Table("Cliente")]
    public partial class Cliente
    {
        public Cliente()
        {
            this.Servico = new HashSet<Servico>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Bairro")]
        public string Bairro { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Cidade")]
        public string Cidade { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Estado")]
        public string Estado { get; set; }

        public virtual ICollection<Servico> Servico { get; set; }

    }
}

