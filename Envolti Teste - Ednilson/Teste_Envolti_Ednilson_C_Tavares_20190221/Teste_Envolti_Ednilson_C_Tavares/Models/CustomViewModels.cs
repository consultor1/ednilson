﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Teste_Envolti_Ednilson_C_Tavares_20190221.Models
{
    public class CustomViewModels
    {
    }
    
    public class RankMensal
    {
        public string Mes { get; set; }
        public int qtdClientes { get; set; }
        public List<TotalCliente> listTotalCliente { get; set; }
    }

    public class TotalCliente
    {
        public string Nome { get; set; }
        public decimal Total { get; set; }
    }

    public class MediaFornecedor
    {
        public string Nome { get; set; }
        public int qtdTipo { get; set; }
        public List<MediaServico> listMediaServico { get; set; }
    }

    public class MediaServico
    {
        public string Tipo { get; set; }
        public decimal Media { get; set; }
    }

    public class RankFrnSemServico
    {
        public string Mes { get; set; }
        public int qtdFrn { get; set; }
        public List<string> listFrnSemServico { get; set; }
    }
}